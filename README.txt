# Request a token when loge in, (using POST method)
# Input #
http://127.0.0.1:8000/api/v1/api-token-auth/ username=some password=30agosto

# Valid Response #
HTTP/1.1 200 OK
Allow: POST, OPTIONS
Content-Length: 52
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:20:50 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Cookie
X-Frame-Options: SAMEORIGIN

{
    "token": "22448c45171631478756a293c9f1c5b4d37bb297"
}
# Invalid Response #
HTTP/1.1 400 Bad Request
Allow: POST, OPTIONS
Content-Length: 68
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:24:30 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Cookie
X-Frame-Options: SAMEORIGIN

{
    "non_field_errors": [
        "Unable to log in with provided credentials."
    ]
}


# Check a Customer or an Employee, (Authorization is sent in the HEADERS)
# Input #
http://127.0.0.1:8000/api/v1/customers/ 'Authorization: Token 22448c45171631478756a293c9f1c5b4d37bb297'
# Valid Response #
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 124
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:23:43 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Accept
X-Frame-Options: SAMEORIGIN

[
    {
        "customer": {
            "photo": null
        },
        "email": "la@gmail.com",
        "first_name": "Luis Angel",
        "last_name": "Montoya Navarro",
        "username": "la"
    }
]

# Invalid Response #
HTTP/1.1 401 Unauthorized
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 27
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:38:47 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Accept
WWW-Authenticate: JWT realm="api"
X-Frame-Options: SAMEORIGIN

{
    "detail": "Invalid token."
}

# Invalid Response 2 #
HTTP/1.1 401 Unauthorized
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 58
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:23:04 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Accept, Cookie
WWW-Authenticate: JWT realm="api"
X-Frame-Options: SAMEORIGIN

{
    "detail": "Authentication credentials were not provided."
}




# Input #
http://127.0.0.1:8000/api/v1/employees/ 'Authorization: Token 22448c45171631478756a293c9f1c5b4d37bb297'
# Valid Response #
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 366
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:37:38 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Accept
X-Frame-Options: SAMEORIGIN

[
    {
        "email": "la@gmail.com",
        "employee": {
            "address": "Diamante #15 Arenales Tapatios",
            "birth_date": "1993-08-31T21:06:36Z",
            "branch_office": {
                "description": "Matriz Tonala",
                "location": "Tonala, Jalisco"
            },
            "dni": null,
            "phone_number": "3317098461",
            "photo": null,
            "role": {
                "description": "Sudo"
            },
            "salary": 60000.0
        },
        "first_name": "Luis Angel",
        "last_name": "Montoya Navarro",
        "username": "la"
    }
]

# Invalid Response #
HTTP/1.1 401 Unauthorized
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 27
Content-Type: application/json
Date: Thu, 03 Oct 2019 19:38:25 GMT
Server: WSGIServer/0.2 CPython/3.6.8
Vary: Accept
WWW-Authenticate: JWT realm="api"
X-Frame-Options: SAMEORIGIN

{
    "detail": "Invalid token."
}



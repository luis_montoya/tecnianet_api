from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class User(AbstractUser):
    def __str__(self):
        return '{}'.format(self.email)


class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    photo = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.user.username)


class BranchOffice(models.Model):
    description = models.CharField(max_length=255, blank=False, null=False)
    location = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return '{}'.format(self.description)


class Role(models.Model):
    description = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return '{}'.format(self.description)


class Employee(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    branch_office = models.ForeignKey(BranchOffice, on_delete=models.DO_NOTHING)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING)
    address = models.CharField(max_length=255, blank=True, null=True)
    salary = models.FloatField(blank=True, null=True, default=0.0)
    dni = models.CharField(max_length=255, blank=True, null=True)
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    birth_date = models.DateTimeField(blank=True, null=True)
    photo = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '{} {} ({})'.format(self.user.first_name, self.user.last_name, self.role.description)

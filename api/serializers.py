from rest_framework import serializers
from .models import User, Customer, Employee, Role, BranchOffice


class RoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ('description', )


class BranchOfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = BranchOffice
        fields = ('description', 'location')


class EmployeeSerializer(serializers.ModelSerializer):
    role = RoleSerializer(required=True)
    branch_office = BranchOfficeSerializer(required=True)

    class Meta:
        model = Employee
        fields = ('address', 'salary', 'dni', 'phone_number', 'birth_date', 'photo', 'role', 'branch_office')


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = ('photo',)


class UserEmployeeSerializer(serializers.HyperlinkedModelSerializer):
    employee = EmployeeSerializer(required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password', 'email', 'username', 'employee')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        employee_data = validated_data.pop('employee')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        Employee.objects.create(user=user, **employee_data)
        return user

    def update(self, instance, validated_data):
        employee_data = validated_data.pop('employee')
        employee = instance.employee

        instance.email = validated_data.get('email', instance.email)
        instance.save()

        employee.address = employee.get('address', employee.address)
        employee.salary = employee.get('salary', employee.salary)
        employee.dni = employee.get('dni', employee.dni)
        employee.phone_number = employee.get('phone_number', employee.phone_number)
        employee.birth_date = employee.get('birth_date', employee.birth_date)
        employee.photo = employee.get('photo', employee.photo)
        employee.role.description = employee.get('role', employee.role.description)
        employee.branch_office.description = employee.get('branch_office', employee.branch_office.description)
        employee.save()

        return instance


class UserCustomerSerializer(serializers.HyperlinkedModelSerializer):
    customer = CustomerSerializer(required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password', 'email', 'username', 'customer')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        customer_data = validated_data.pop('customer')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        Customer.objects.create(user=user, **customer_data)
        return user

    def update(self, instance, validated_data):
        customer_data = validated_data.pop('customer')
        customer = instance.customer

        instance.email = validated_data.get('email', instance.email)
        instance.save()

        customer.photo = customer.get('photo', customer.photo)
        customer.save()

        return instance

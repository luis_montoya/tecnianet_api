from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import User, Customer
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )

    queryset = User.objects.all()
    serializer_class = UserEmployeeSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )

    queryset = User.objects.all()
    serializer_class = UserCustomerSerializer


class BranchOfficeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )

    queryset = BranchOffice.objects.all()
    serializer_class = BranchOfficeSerializer

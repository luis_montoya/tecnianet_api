from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from .views import *


"""
The DefaultRouter class will define the standard REST (GET, POST, PUT, DELETE … ) 
"""

router = routers.DefaultRouter()
router.register(r'employees', UserViewSet)
router.register(r'customers', CustomerViewSet)
router.register(r'branch_office', BranchOfficeViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]
